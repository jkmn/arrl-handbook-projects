EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_SPST SW?
U 1 1 5F897ED1
P 3300 2700
AR Path="/5F897ED1" Ref="SW?"  Part="1" 
AR Path="/5F891ECE/5F897ED1" Ref="SW?"  Part="1" 
F 0 "SW?" H 3300 2935 50  0000 C CNN
F 1 "SW_SPST" H 3300 2844 50  0000 C CNN
F 2 "" H 3300 2700 50  0001 C CNN
F 3 "~" H 3300 2700 50  0001 C CNN
	1    3300 2700
	1    0    0    -1  
$EndComp
$Comp
L power:AC #PWR?
U 1 1 5F897ED7
P 2950 2400
AR Path="/5F897ED7" Ref="#PWR?"  Part="1" 
AR Path="/5F891ECE/5F897ED7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2950 2300 50  0001 C CNN
F 1 "AC" H 2950 2675 50  0000 C CNN
F 2 "" H 2950 2400 50  0001 C CNN
F 3 "" H 2950 2400 50  0001 C CNN
	1    2950 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2400 2950 2700
Wire Wire Line
	2950 2700 3100 2700
$Comp
L Device:Fuse F?
U 1 1 5F897EDF
P 3750 2700
AR Path="/5F897EDF" Ref="F?"  Part="1" 
AR Path="/5F891ECE/5F897EDF" Ref="F?"  Part="1" 
F 0 "F?" V 3553 2700 50  0000 C CNN
F 1 "Fuse" V 3644 2700 50  0000 C CNN
F 2 "" V 3680 2700 50  0001 C CNN
F 3 "~" H 3750 2700 50  0001 C CNN
	1    3750 2700
	0    1    1    0   
$EndComp
$Comp
L Device:Transformer_1P_SS T?
U 1 1 5F897EE5
P 4550 2900
AR Path="/5F897EE5" Ref="T?"  Part="1" 
AR Path="/5F891ECE/5F897EE5" Ref="T?"  Part="1" 
F 0 "T?" H 4550 3281 50  0000 C CNN
F 1 "Transformer_1P_SS" H 4550 3190 50  0000 C CNN
F 2 "" H 4550 2900 50  0001 C CNN
F 3 "~" H 4550 2900 50  0001 C CNN
	1    4550 2900
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5F897EEB
P 4100 3150
AR Path="/5F897EEB" Ref="#PWR?"  Part="1" 
AR Path="/5F891ECE/5F897EEB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4100 2900 50  0001 C CNN
F 1 "Earth" H 4100 3000 50  0001 C CNN
F 2 "" H 4100 3150 50  0001 C CNN
F 3 "~" H 4100 3150 50  0001 C CNN
	1    4100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3100 4100 3100
Wire Wire Line
	4100 3100 4100 3150
Wire Wire Line
	3900 2700 3950 2700
Wire Wire Line
	3500 2700 3600 2700
$Comp
L Device:Transformer_1P_SS T?
U 1 1 5F897EF5
P 4550 4050
AR Path="/5F897EF5" Ref="T?"  Part="1" 
AR Path="/5F891ECE/5F897EF5" Ref="T?"  Part="1" 
F 0 "T?" H 4550 4431 50  0000 C CNN
F 1 "Transformer_1P_SS" H 4550 4340 50  0000 C CNN
F 2 "" H 4550 4050 50  0001 C CNN
F 3 "~" H 4550 4050 50  0001 C CNN
	1    4550 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3850 4150 3850
Connection ~ 3950 2700
Wire Wire Line
	3950 2700 4150 2700
Wire Wire Line
	3950 2700 3950 3850
$Comp
L power:Earth #PWR?
U 1 1 5F897EFF
P 4100 4300
AR Path="/5F897EFF" Ref="#PWR?"  Part="1" 
AR Path="/5F891ECE/5F897EFF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4100 4050 50  0001 C CNN
F 1 "Earth" H 4100 4150 50  0001 C CNN
F 2 "" H 4100 4300 50  0001 C CNN
F 3 "~" H 4100 4300 50  0001 C CNN
	1    4100 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 4250 4100 4250
Wire Wire Line
	4100 4250 4100 4300
$Comp
L Connector:TestPoint TP?
U 1 1 5F897F07
P 4950 4050
AR Path="/5F897F07" Ref="TP?"  Part="1" 
AR Path="/5F891ECE/5F897F07" Ref="TP?"  Part="1" 
F 0 "TP?" V 4904 4238 50  0000 L CNN
F 1 "TestPoint" V 4995 4238 50  0000 L CNN
F 2 "" H 5150 4050 50  0001 C CNN
F 3 "~" H 5150 4050 50  0001 C CNN
	1    4950 4050
	0    1    1    0   
$EndComp
$Comp
L Device:D_Bridge_+-AA D?
U 1 1 5F897F0D
P 5850 2950
AR Path="/5F897F0D" Ref="D?"  Part="1" 
AR Path="/5F891ECE/5F897F0D" Ref="D?"  Part="1" 
F 0 "D?" H 6194 2996 50  0000 L CNN
F 1 "D_Bridge_+-AA" H 6194 2905 50  0000 L CNN
F 2 "" H 5850 2950 50  0001 C CNN
F 3 "~" H 5850 2950 50  0001 C CNN
	1    5850 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Bridge_+-AA D?
U 1 1 5F897F13
P 6050 4100
AR Path="/5F897F13" Ref="D?"  Part="1" 
AR Path="/5F891ECE/5F897F13" Ref="D?"  Part="1" 
F 0 "D?" H 6394 4146 50  0000 L CNN
F 1 "D_Bridge_+-AA" H 6394 4055 50  0000 L CNN
F 2 "" H 6050 4100 50  0001 C CNN
F 3 "~" H 6050 4100 50  0001 C CNN
	1    6050 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2700 5550 2650
Wire Wire Line
	5550 2650 5850 2650
Wire Wire Line
	4950 2700 5550 2700
Wire Wire Line
	4950 3100 5200 3100
Wire Wire Line
	5200 3100 5200 3250
Wire Wire Line
	5200 3250 5850 3250
Wire Wire Line
	4950 3850 5450 3850
Wire Wire Line
	5450 3850 5450 3800
Wire Wire Line
	5450 3800 6050 3800
Wire Wire Line
	4950 4250 4950 4400
Wire Wire Line
	4950 4400 6050 4400
$Comp
L Regulator_Linear:LM317L_TO92 U?
U 1 1 5F89AAE6
P 6600 5150
F 0 "U?" H 6600 5392 50  0000 C CNN
F 1 "LM317L_TO92" H 6600 5301 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 6600 5375 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/snvs775k/snvs775k.pdf" H 6600 5150 50  0001 C CNN
	1    6600 5150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
